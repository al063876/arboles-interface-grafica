
package javaapplication34;

/**
 *
 * @author ameri
 */
public class NodoArbol {
    int dato;
    String nombre;
    NodoArbol hijoizquierdo, hijoderecho;
    public NodoArbol (int d, String s){
        this.dato = d;
        this.nombre = s;
        this.hijoizquierdo = null;
        this.hijoderecho = null;
    }
    @Override
    public String toString(){
        return nombre + "El dato es : "+dato;
    }
}
