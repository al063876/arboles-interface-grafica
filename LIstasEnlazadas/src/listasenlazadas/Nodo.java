
package listasenlazadas;

public class Nodo {
   public String dato;    // Valor a almacenar
    public Nodo siguiente;  // Puntero, del mismo tipo de la clase
    
    // Constructor para insertar el dato
    public Nodo(String d){
        this.dato = d;
    }
    // Constructor para insertar al inicio de la lista
    public Nodo(String d, Nodo n){
        dato = d;
        siguiente = n;
    }
}

