package listasenlazadas;


public class Listas {

    protected Nodo inicio, fin;   // Punteros para donde esta el inicio y el fin
    public Listas(){
        inicio = null;
        fin    = null;
    }
    
    public void agregarAlInicio(String elemento){
        inicio = new Nodo(elemento, inicio);
        if (fin == null){
            fin = inicio;
        }
    }
    public void mostrarListaEnlazada(){
        Nodo recorrer = inicio;
        System.out.println("");
        while (recorrer != null){
            System.out.print("["+ recorrer.dato+"] -->");
            recorrer = recorrer.siguiente;
        }
        System.out.println("");
    }
    
    // Metodo para eliminar un nodo del inicio
    public String borrarDelInicio(){
        String elemento = inicio.dato;
        if (inicio == fin){
            inicio = null;
            fin    = null;
        } else {
            inicio = inicio.siguiente;
        }
        return elemento;
    }
    
}

